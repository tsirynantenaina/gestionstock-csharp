# Gestion de stock en CSharp Desktop
L'Application permet de :
* Ajouter, modifier, supprimer des produits
* Ajouter , modifier, supprimer des clients
* Gerer automatiquement le stock des produits
* Afficher graphiquement la quentité des produits en stock
* Gerer le panier d'achat d'un client lors de l'achat
* Gerer les commandes des clients
* Imprimer une facture


## Prérequis et installation
* L'application marche uniquement sur windows et linux
* il est necessaire d'installer visual studio sur votre machine

## Base de donnees 
* On utilise la base de donnees interne du visual studuio

## Commant lancer l'application?
* Ouvrir le projet sur visual studio
* lancer le projet

## Authentification Par defaut
* Utilisateur:
```
admin

```
* Mot de passe:
```
admin

```
