﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace monAppli
{
    public partial class acceuil : Form
    {
        SqlCommand cmd;
        SqlDataReader dr;
        SqlConnection cnx;
        public acceuil()
        {
            InitializeComponent();
            cnx = new SqlConnection();
            cnx.ConnectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=F:\C#\monAppli\monAppli\base.mdf;Integrated Security=True";
            try
            {
                cnx.Open();
                String countLogin = "SELECT count(*) from login";
                SqlCommand cmd = new SqlCommand(countLogin, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                if(dr.Read())
                {
                    user.Text = dr[0].ToString();   
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            try
            {
                cnx.Open();
                String countFrns = "SELECT count(*) from fournisseur";
                SqlCommand cmd = new SqlCommand(countFrns, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    frns.Text = dr[0].ToString();
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            try
            {
                cnx.Open();
                String countProd = "SELECT count(*) from produit";
                SqlCommand cmd = new SqlCommand(countProd, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    prod.Text = dr[0].ToString();
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            try
            {
                cnx.Open();
                String countCli = "SELECT count(*) from client";
                SqlCommand cmd = new SqlCommand(countCli, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    cli.Text = dr[0].ToString();
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            try
            {
                cnx.Open();
                String countPan = "SELECT sum(qteAchat) from achat";
                SqlCommand cmd = new SqlCommand(countPan, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    panier.Text = dr[0].ToString();
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            try
            {
                cnx.Open();
                String countProd = "SELECT count(*) from achatID";
                SqlCommand cmd = new SqlCommand(countProd, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    Achat.Text = dr[0].ToString();
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void acceuil_Load(object sender, EventArgs e)
        {
            cmd = new SqlCommand("select * from produit ", cnx);
            try
            {
                cnx.Open();
                dr = cmd.ExecuteReader();
                this.chart1.Series.Clear();
                this.chart1.Series.Add("Seris1");
                while (dr.Read())
                {
                    this.chart1.Series["Seris1"].Points.AddXY(dr["designation"].ToString(), dr["qte"].ToString());

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            dr.Dispose();
            cnx.Close();
        }

        private void acceuilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            acceuil2 AC= new acceuil2();
            AC.Show();
            this.panelChange.Controls.Clear();
            this.panelChange.Controls.Add(AC);

        }

        private void fOURNISSEURToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fournisseur f = new fournisseur();
            f.Show();
            this.panelChange.Controls.Clear();
            this.panelChange.Controls.Add(f);

        }

        private void pRODUITToolStripMenuItem_Click(object sender, EventArgs e)
        {
            produit p = new produit();
            p.Show();
            this.panelChange.Controls.Clear();
            this.panelChange.Controls.Add(p);

        }

        private void rEAPROVISIONNEMENTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reaprov r = new reaprov();
            r.Show();
            this.panelChange.Controls.Clear();
            this.panelChange.Controls.Add(r);
        }

        private void cOMMANDEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            client cli = new client();
            cli.Show();
            this.panelChange.Controls.Clear();
            this.panelChange.Controls.Add(cli);
        }

        private void aCHATToolStripMenuItem_Click(object sender, EventArgs e)
        {
            achat ach = new achat();
            ach.Show();
            this.panelChange.Controls.Clear();
            this.panelChange.Controls.Add(ach);

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void panelChange_Paint(object sender, PaintEventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void factureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            facture fct = new facture();
           fct.Show();
            this.panelChange.Controls.Clear();
            this.panelChange.Controls.Add(fct);
        }

        private void tousTransactionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tousAchat tt = new tousAchat();
            tt.Show();
            this.panelChange.Controls.Clear();
            this.panelChange.Controls.Add(tt);
        }

        
    }
}
