﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace monAppli
{
    public partial class achat : UserControl
    {
        SqlCommand cmd;
        SqlDataReader dr;
        SqlConnection cnx;
        public achat()
        {
            InitializeComponent();
            cnx = new SqlConnection();
            cnx.ConnectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=F:\C#\monAppli\monAppli\base.mdf;Integrated Security=True";
            combo1();
            combo2();
            actualiser();
            qteProd(); somme();
            
        }
        public void combo1()
        {
            try
            {
                cnx.Open();
                String liste = "SELECT * FROM Client";
                SqlCommand cmd = new SqlCommand(liste, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    comboBox1.Items.Add(dr[0].ToString() + " (" + dr[1].ToString() + ")");
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
        public void combo2()
        {
            try
            {
                cnx.Open();
                String liste = "SELECT * FROM produit";
                SqlCommand cmd = new SqlCommand(liste, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    comboBox2.Items.Add(dr[0].ToString() + " (" + dr[1].ToString() + ")");
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
        public void Affiche_PU_Qte()
        {
            if (comboBox2.Text !="") {
                try
                {
                    string[] vals = comboBox2.Text.Split('(');
                    cnx.Open();
                    String liste = "SELECT qte , pu FROM produit where idProd="+vals[0]+"";
                    SqlCommand cmd = new SqlCommand(liste, cnx);
                    cmd.ExecuteNonQuery();
                    dr = cmd.ExecuteReader();
                    if(dr.Read())
                    {
                        label8.Text = dr[0].ToString();
                        label9.Text = dr[1].ToString();
                    }
                    dr.Close();
                    cnx.Close();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
           
        }

        public void calculAuto() {
            if (comboBox2.Text != "")
            {
                if (qteAchat.Text != "")
                {
                    int qte = int.Parse(qteAchat.Text);
                    int prix = int.Parse(label9.Text);
                    int stotal = qte * prix;
                    string tot = stotal.ToString();
                    sousTotal.Text = tot;
                }

            }

            
        }
        public void actualiser() {
            try
            {
                cnx.Open();
                dataGridView1.Rows.Clear();
                string liste = "select * from panier";
                SqlCommand cmd = new SqlCommand(liste, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dataGridView1.Rows.Add(dr[1].ToString(), dr[2].ToString(),
                        dr[3].ToString(), dr[4].ToString(), dr[0].ToString());
                }
                

                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
        public void somme() {
            try
            {
                cnx.Open();
                string som = "select sum(sousTotal) from panier";
                SqlCommand cmd = new SqlCommand(som, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    label22.Text = dr[0].ToString();
                }


                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        
        }

        public void qteProd()
        {
            try
            {
                cnx.Open();
                string som2 = "select sum(QtePan) from panier";
                SqlCommand cmd = new SqlCommand(som2, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    label23.Text = dr[0].ToString();
                }


                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }
        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void achat_Load(object sender, EventArgs e)
        {
           
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "") {
                MessageBox.Show("aucun client selectionné");
            }else

            if (comboBox2.Text == "")
            {
                MessageBox.Show("aucun produit selectionné");
            }
            else {

                int qte = int.Parse(qteAchat.Text);
                int stock = int.Parse(label8.Text);
                if (qte > stock)
                {
                    MessageBox.Show("stock insuffisant");
                }
                else
                {
                    try
                    {
                        string[] idCli = comboBox1.Text.Split('(');
                        string[] idProd = comboBox2.Text.Split('(');
                        cnx.Open();
                        String insert = "INSERT INTO panier( id_Cli , id_Prod , QtePan , sousTotal) VALUES('" + idCli[0] + "','" + idProd[0] + "','" + qteAchat.Text + "','" + sousTotal.Text + "')";
                        SqlCommand cmd = new SqlCommand(insert, cnx);
                        cmd.ExecuteNonQuery();
                        int q = int.Parse(qteAchat.Text);
                        String update = "UPDATE produit SET qte=(qte -" + q + ")  where idProd=" + idProd[0] + "";
                        SqlCommand cmdUpdate = new SqlCommand(update, cnx);
                        cmdUpdate.ExecuteNonQuery();

                        cnx.Close();
                        actualiser();

                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                    qteProd(); somme(); comboBox2.Text = ""; label8.Text = ""; label9.Text = ""; label15.Text=""; qteAchat.Text = "0"; sousTotal.Text = "";

                }
            }
            
           
            
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Affiche_PU_Qte();
        }

        private void qteAchat_KeyUp(object sender, KeyEventArgs e)
        {
            calculAuto();
        }

        private void qteAchat_Click(object sender, EventArgs e)
        {
            calculAuto();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
        }
        public void vider1() {
            label11.Text = ""; label12.Text = ""; label13.Text = ""; label14.Text = ""; label15.Text = "";
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (label15.Text == "")
            {
                MessageBox.Show("aucun ligne selectionné");
            }
            else {
                try
                {

                    {
                        cnx.Open();
                        String delete = "DELETE FROM panier where idPanier=" + label15.Text + "";
                        SqlCommand cmd = new SqlCommand(delete, cnx);
                        cmd.ExecuteNonQuery();
                        int q = int.Parse(label13.Text);
                        String update = "UPDATE produit SET qte=(qte +" + q + ")  where idProd=" + label12.Text + "";
                        SqlCommand cmdUpdate = new SqlCommand(update, cnx);
                        cmdUpdate.ExecuteNonQuery();

                        cnx.Close();
                        actualiser();
                    }


                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
                qteProd(); somme();

                if (dataGridView1.CurrentRow != null)
                {
                    if (dataGridView1.CurrentRow.Cells[0].Value != null)
                    {
                        label11.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                        label12.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                        label13.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                        label14.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                        label15.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();

                    }
                    else { vider1(); }
                }
                else { vider1(); }
            }
           
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String id = "";
            try
            {
                String date = (DateTime.Now).ToString();
                cnx.Open();
                String insertDate = "INSERT INTO AchatID(dateAchat) VALUES('"+date+"')";
                SqlCommand cmd = new SqlCommand(insertDate, cnx);
                cmd.ExecuteNonQuery();
                cnx.Close();
                actualiser();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            try
            {
                cnx.Open();
                String idMax = "SELECT max(idAchat) from AchatID";
                SqlCommand cmd = new SqlCommand(idMax, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    id = dr[0].ToString();
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }


            String idCli = "";
            
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                idCli = dataGridView1.Rows[i].Cells[0].Value.ToString();
                String idProd = dataGridView1.Rows[i].Cells[1].Value.ToString();
                String qte = dataGridView1.Rows[i].Cells[2].Value.ToString();
                String tot = dataGridView1.Rows[i].Cells[3].Value.ToString();
                try{
                cnx.Open();
                String insertAchat = "INSERT INTO Achat(idAchat , idCliAchat , idProdAchat , qteAchat , sousTotalAchat) VALUES('" + id + "','" + idCli + "','" + idProd + "','" + qte + "','" + tot + "')";
                SqlCommand cmd = new SqlCommand(insertAchat, cnx);
                cmd.ExecuteNonQuery();
                cnx.Close();
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }

            }
            try{
            cnx.Open();
            String delete = " DELETE FROM panier";
            SqlCommand cmd = new SqlCommand(delete, cnx);
            cmd.ExecuteNonQuery();
            cnx.Close();
            actualiser();
           

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            try
            {

                cnx.Open();
                String update = "UPDATE  AchatID SET idClient="+idCli+" where idAchat="+id+"";
                SqlCommand cmd = new SqlCommand(update, cnx);
                cmd.ExecuteNonQuery();
                cnx.Close();
                actualiser();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            label23.Text = ""; label22.Text = "";
            MessageBox.Show("acahat valider");


           
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dataGridView1.CurrentRow != null)
            {
                if (dataGridView1.CurrentRow.Cells[0].Value != null)
                {
                    label11.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    label12.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                    label13.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                    label14.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                    label15.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();

                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            String date = (DateTime.Now).ToString();
            MessageBox.Show(date);
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void qteAchat_KeyPress(object sender, KeyPressEventArgs e)
        {
            calculAuto();
        }

    }
}
