﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace monAppli
{
    public partial class fournisseur : UserControl
    {
        SqlCommand cmd;
        SqlDataReader dr;
        SqlConnection cnx;
        public fournisseur()
        {
            InitializeComponent();
            cnx = new SqlConnection();
            cnx.ConnectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=F:\C#\monAppli\monAppli\base.mdf;Integrated Security=True";
            try
            {
                cnx.Open();
                String liste = "SELECT * FROM fournisseur";
                SqlCommand cmd = new SqlCommand(liste, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(),
                        dr[3].ToString());
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
        public void actualiser()
        {
            try
            {
                cnx.Open();
                dataGridView1.Rows.Clear();
                String liste = "SELECT * FROM fournisseur";
                SqlCommand cmd = new SqlCommand(liste, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(),
                        dr[3].ToString());
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void fournisseur_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
           
            
                try
                {
                    cnx.Open();
                    String insert = "INSERT INTO fournisseur(nom_Frns,adrs_Frns,tel_Frns) VALUES('" + textnom.Text + "','" + textAdresse.Text + "','" + textTel.Text + "')";
                    SqlCommand cmd = new SqlCommand(insert, cnx);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Donnees Inserer");
                    cnx.Close();
                    actualiser();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            
            
            
        }
        

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                if (dataGridView1.CurrentRow.Cells[0].Value != null)
                {
                    label6.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    textnom.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                    textAdresse.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                    textTel.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                    button2.Enabled = false;
                    button3.Enabled = true;
                    button4.Enabled = true;

                    
                    label1.Text = "ID";
                   

                   
                    
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Modifier donnees?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                try
                {
                    cnx.Open();
                    String update = "UPDATE  fournisseur SET nom_Frns ='" + textnom.Text + "',adrs_Frns='" + textAdresse.Text + "',tel_Frns='" + textTel.Text + "' where id_Frns=" + label6.Text + "";
                    SqlCommand cmd = new SqlCommand(update, cnx);
                    cmd.ExecuteNonQuery();
                    cnx.Close();
                    actualiser();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Voulez-vous supprimer?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                

                cnx.Open();
                String delete = " DELETE FROM fournisseur WHERE id_Frns=" + label6.Text + "";
                SqlCommand cmd = new SqlCommand(delete, cnx);
                cmd.ExecuteNonQuery();
                cnx.Close();
                actualiser();
            }
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Serch.Text != "")
            {
                try
                {
                    cnx.Open();
                    dataGridView1.Rows.Clear();
                    String liste = "select * from fournisseur where Id_Frns like '%" + Serch.Text + "%' or nom_Frns like '%" + Serch.Text + "%' or adrs_Frns like '%" + Serch.Text + "%' or tel_Frns like '%" + Serch.Text + "%' ";
                    SqlCommand cmd = new SqlCommand(liste, cnx);
                    cmd.ExecuteNonQuery();
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(),
                            dr[3].ToString());
                    }
                    dr.Close();
                    cnx.Close();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
            else {

                try
                {
                    cnx.Open();
                    dataGridView1.Rows.Clear();
                    String liste = "SELECT * FROM fournisseur";
                    SqlCommand cmd = new SqlCommand(liste, cnx);
                    cmd.ExecuteNonQuery();
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(),
                            dr[3].ToString());
                    }
                    dr.Close();
                    cnx.Close();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void Serch_KeyUp(object sender, KeyEventArgs e)
        {
            if (Serch.Text != "")
            {
                try
                {
                    cnx.Open();
                    dataGridView1.Rows.Clear();
                    String liste = "select * from fournisseur where Id_Frns like '%" + Serch.Text + "%' or nom_Frns like '%" + Serch.Text + "%' or adrs_Frns like '%" + Serch.Text + "%' or tel_Frns like '%" + Serch.Text + "%' ";
                    SqlCommand cmd = new SqlCommand(liste, cnx);
                    cmd.ExecuteNonQuery();
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(),
                            dr[3].ToString());
                    }
                    dr.Close();
                    cnx.Close();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
            else
            {

                try
                {
                    cnx.Open();
                    dataGridView1.Rows.Clear();
                    String liste = "SELECT * FROM fournisseur";
                    SqlCommand cmd = new SqlCommand(liste, cnx);
                    cmd.ExecuteNonQuery();
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(),
                            dr[3].ToString());
                    }
                    dr.Close();
                    cnx.Close();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void Serch_TextChanged(object sender, EventArgs e)
        {

        }

        private void textTel_AcceptsTabChanged(object sender, EventArgs e)
        {

        }

        private void textTel_VisibleChanged(object sender, EventArgs e)
        {

        }

        private void textnom_Click(object sender, EventArgs e)
        {
            
        }

        private void textnom_TextChanged(object sender, EventArgs e)
        {

        }

        private void textAdresse_TextChanged(object sender, EventArgs e)
        {

        }

        private void textnom_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void textAdresse_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void textTel_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            button2.Enabled = true;
            button3.Enabled = false;
            button4.Enabled = false;
            label6.Text = "";
            label1.Text = "Veuilllez remplir le formulaire puis cliquer 'Ajouter'";
            textnom.Text = "";
            textAdresse.Text = "";
            textTel.Text = "";
        }

        private void textTel_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            
        }

        private void textTel_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
