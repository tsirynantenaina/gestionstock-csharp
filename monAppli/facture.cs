﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace monAppli
{
    public partial class facture : UserControl
    {
        SqlCommand cmd;
        SqlDataReader dr;
        SqlConnection cnx;
        public facture()
        {
            InitializeComponent();
            cnx = new SqlConnection();
            cnx.ConnectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=F:\C#\monAppli\monAppli\base.mdf;Integrated Security=True";
           
            try
            {
                dataGridView2.Rows.Clear();
                cnx.Open();
                String req = "SELECT AchatID.* , client.* from AchatID ,client where AchatID.idClient=client.idCli order by AchatID.idAchat desc";
                SqlCommand cmd = new SqlCommand(req, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dataGridView2.Rows.Add(dr["idAchat"].ToString(), dr["nomCli"].ToString(), dr["dateAchat"].ToString());
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
             
            
            

            


        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void facture_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            printDocument1.Print();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Bitmap bm = new Bitmap(this.panel1.Width, this.panel1.Height);
            panel1.DrawToBitmap(bm, new Rectangle(0, 0, this.panel1.Width, this.panel1.Height));
            e.Graphics.DrawImage(bm, 0, 0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.Document = printDocument1;
            printPreviewDialog1.Show();
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
          
            
        }

        private void dataGridView2_SelectionChanged(object sender, EventArgs e)
        {
            String idAchat = "";
            if (dataGridView2.CurrentRow != null)
            {
                if (dataGridView2.CurrentRow.Cells[0].Value != null)
                {
                    idAchat = dataGridView2.CurrentRow.Cells[0].Value.ToString();

                    try
                    {
                        cnx.Open();
                        dataGridView1.Rows.Clear();
                        String liste = "select produit.designation , fournisseur.nom_Frns , produit.pu , Achat.qteAchat , Achat.sousTotalAchat, client.idCli , client.nomCli , client.adrsCli , client.telCli , AchatID.dateAchat from produit, AchatID, fournisseur , Achat , client where produit.idProd=Achat.idProdAchat and fournisseur.Id_Frns = produit.idFrns and client.idCli = Achat.idCliAchat  and AchatID.IdAchat = Achat.IdAchat and Achat.IdAchat=" + idAchat + "";
                        SqlCommand cmd = new SqlCommand(liste, cnx);
                        cmd.ExecuteNonQuery();
                        dr = cmd.ExecuteReader();
                        String idClient = "";
                        String nomClient = "";
                        String telClient = "";
                        String AdrsClient = "";
                        String dt = "";
                        String dateActu1 = (DateTime.Now).ToString("dd-mm-yy");
                        dateActu.Text = "Facturé le : " + dateActu1 + "";
                        while (dr.Read())
                        {
                            dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(),
                            dr[3].ToString(), dr[4].ToString());
                            idClient = dr[5].ToString();
                            nomClient = dr[6].ToString();
                            AdrsClient = dr[7].ToString();
                            telClient = dr[8].ToString();
                            dt = dr[9].ToString();

                        }

                        nomCli.Text = "" + nomClient + "";
                        adrsCli.Text = "" + AdrsClient + "";
                        telCli.Text = "" + telClient + "";
                        date.Text = "" + dt + "";
                        dr.Close();
                        cnx.Close();

                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }

                    try
                    {
                        cnx.Open();
                        String countProd = "SELECT sum(qteAchat) from achat where idAchat=" + idAchat + "";
                        SqlCommand cmd = new SqlCommand(countProd, cnx);
                        cmd.ExecuteNonQuery();
                        dr = cmd.ExecuteReader();
                        if (dr.Read())
                        {
                            qteProd.Text = "" + dr[0].ToString() + "";
                        }
                        dr.Close();
                        cnx.Close();

                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }

                    try
                    {
                        cnx.Open();
                        String countProd = "SELECT sum(sousTotalAchat) from achat where idAchat=" + idAchat + "";
                        SqlCommand cmd = new SqlCommand(countProd, cnx);
                        cmd.ExecuteNonQuery();
                        dr = cmd.ExecuteReader();
                        if (dr.Read())
                        {
                            tht.Text = "" + dr[0].ToString() + " Fmg";
                            float tot = float.Parse(dr[0].ToString());
                            float tva = (tot * 20) / 100;
                            float total1 = tot + tva;
                            valeurAjoute.Text = "" + tva.ToString() + " Fmg";
                            total.Text = "" + total1.ToString() + " Fmg";


                        }
                        dr.Close();
                        cnx.Close();

                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }

                }
            }
        }
    }
}
