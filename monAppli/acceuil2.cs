﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace monAppli
{
    public partial class acceuil2 : UserControl
    {
        SqlCommand cmd;
        SqlDataReader dr;
        SqlConnection cnx;
        public acceuil2()
        {
            InitializeComponent();
            cnx = new SqlConnection();
            cnx.ConnectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=F:\C#\monAppli\monAppli\base.mdf;Integrated Security=True";
            try
            {
                cnx.Open();
                String countLogin = "SELECT count(*) from login";
                SqlCommand cmd = new SqlCommand(countLogin, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    user.Text = dr[0].ToString();
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            try
            {
                cnx.Open();
                String countFrns = "SELECT count(*) from fournisseur";
                SqlCommand cmd = new SqlCommand(countFrns, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    frns.Text = dr[0].ToString();
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            try
            {
                cnx.Open();
                String countProd = "SELECT count(*) from produit";
                SqlCommand cmd = new SqlCommand(countProd, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    prod.Text = dr[0].ToString();
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            try
            {
                cnx.Open();
                String countCli = "SELECT count(*) from client";
                SqlCommand cmd = new SqlCommand(countCli, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    cli.Text = dr[0].ToString();
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            try
            {
                cnx.Open();
                String countPan = "SELECT sum(qteAchat) from achat";
                SqlCommand cmd = new SqlCommand(countPan, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    panier.Text = dr[0].ToString();
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            try
            {
                cnx.Open();
                String countProd = "SELECT count(*) from achatID";
                SqlCommand cmd = new SqlCommand(countProd, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    Achat.Text = dr[0].ToString();
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void chart1_Click(object sender, EventArgs e)
        {
           
        }

        private void panelChange_Enter(object sender, EventArgs e)
        {
            
        }

        private void acceuil2_Load(object sender, EventArgs e)
        {
            cmd = new SqlCommand("select * from produit ", cnx);
            try
            {
                cnx.Open();
                dr = cmd.ExecuteReader();
                this.chart1.Series.Clear();
                this.chart1.Series.Add("Seris1");
                while (dr.Read())
                {
                    this.chart1.Series["Seris1"].Points.AddXY(dr["designation"].ToString(), dr["qte"].ToString());

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            dr.Dispose();
            cnx.Close();
        }

        private void panel8_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
