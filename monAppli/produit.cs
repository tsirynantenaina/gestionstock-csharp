﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace monAppli
{
    public partial class produit : UserControl
    {
        SqlCommand cmd;
        SqlDataReader dr;
        SqlConnection cnx;
        public produit()
        {
            InitializeComponent();
            cnx = new SqlConnection();
            cnx.ConnectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=F:\C#\monAppli\monAppli\base.mdf;Integrated Security=True";
            combo();
            tableau();
        }
        public void combo(){
            try
            {
                cnx.Open();
                String liste = "SELECT * FROM fournisseur";
                SqlCommand cmd = new SqlCommand(liste, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    comboBox1.Items.Add(dr[0].ToString()+" ("+dr[1].ToString()+")");
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
        public void tableau() {
            try
            {
                cnx.Open();
                //dataGridView1.Rows.Clear();
                String liste = "SELECT produit.idProd , produit.designation , fournisseur.nom_Frns, produit.qte , produit.pu , fournisseur.id_Frns  from produit , fournisseur  where produit.idFrns = fournisseur.id_Frns";
                SqlCommand cmd = new SqlCommand(liste, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[5].ToString() + " (" + dr[2].ToString() + ")",
                        dr[3].ToString(), dr[4].ToString());
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }

        public void actualiser()
        {
            try
            {
                cnx.Open();
                dataGridView1.Rows.Clear();
                String liste = "SELECT produit.idProd , produit.designation , fournisseur.nom_Frns, produit.qte , produit.pu , fournisseur.id_Frns  from produit , fournisseur  where produit.idFrns = fournisseur.id_Frns";
                SqlCommand cmd = new SqlCommand(liste, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[5].ToString() + " (" + dr[2].ToString() + ")",
                        dr[3].ToString(), dr[4].ToString());
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void produit_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "Veuilllez remplir le formulaire puis cliquer 'Ajouter'";
            button3.Enabled = false;
            button4.Enabled = false;
            label6.Text = "";
            design.Text ="";
            qte.Text = "";
            prixUnitaire.Text ="";
            comboBox1.Text="";
            button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (button2.Text == "Ajouter")
            {
                try
                {
                    string[] vals = comboBox1.Text.Split('(');
                    cnx.Open();
                    String insert = "INSERT INTO produit(designation,qte,pu,idFrns) VALUES('" + design.Text + "','" + qte.Text + "','" + prixUnitaire.Text + "','" + vals[0] + "')";
                    SqlCommand cmd = new SqlCommand(insert, cnx);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Donnees Inserer");
                    cnx.Close();
                    actualiser();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
            else {

                label6.Text = "";
                label1.Text = "";
                design.Text = "";
                qte.Text = "";
                prixUnitaire.Text = "";
                comboBox1.Text = "";

                design.BackColor = Color.White;
                qte.BackColor = Color.White;
                prixUnitaire.BackColor = Color.White;
                comboBox1.BackColor = Color.White;

                design.ForeColor = Color.Black;
                qte.ForeColor = Color.Black;
                prixUnitaire.ForeColor = Color.Black;
                comboBox1.ForeColor = Color.Black;

                design.BorderStyle = BorderStyle.FixedSingle;
                qte.BorderStyle = BorderStyle.FixedSingle;
                prixUnitaire.BorderStyle = BorderStyle.FixedSingle;
                
                button2.Text = "Ajouter";
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                if (dataGridView1.CurrentRow.Cells[0].Value != null)
                {
                    label6.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    design.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                    comboBox1.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                    qte.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                    prixUnitaire.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
                    label1.Text = "ID";
                    button2.Enabled = false;
                    button3.Enabled = true;
                    button4.Enabled = true;

                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Modifier donnees?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                try
                {
                    string[] vals = comboBox1.Text.Split('(');
                    cnx.Open();
                    String update = "UPDATE  produit SET designation ='" + design.Text + "',qte='" + qte.Text + "',pu='" + prixUnitaire.Text + "', idFrns="+vals[0]+" where idProd=" + label6.Text + "";
                    SqlCommand cmd = new SqlCommand(update, cnx);
                    cmd.ExecuteNonQuery();
                    cnx.Close();
                    actualiser();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Voulez-vous supprimer?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {

                cnx.Open();
                String delete = " DELETE FROM produit WHERE idProd=" + label6.Text + "";
                SqlCommand cmd = new SqlCommand(delete, cnx);
                cmd.ExecuteNonQuery();
                cnx.Close();
                actualiser();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void rech_KeyUp(object sender, KeyEventArgs e)
        {
            if (rech.Text != "")
            {
                try
                {
                    cnx.Open();
                    dataGridView1.Rows.Clear();
                    String liste = "SELECT produit.idProd , produit.designation , fournisseur.nom_Frns, produit.qte , produit.pu , fournisseur.id_Frns  from produit , fournisseur  where  produit.designation like '%" + rech.Text + "%' or fournisseur.nom_Frns like '%" + rech.Text + "%' or  produit.qte like '%" + rech.Text + "%' or  produit.pu like '%" + rech.Text + "%' AND  produit.idFrns = fournisseur.id_Frns";
                    SqlCommand cmd = new SqlCommand(liste, cnx);
                    cmd.ExecuteNonQuery();
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[5].ToString() + " (" + dr[2].ToString() + ")",
                            dr[3].ToString(), dr[4].ToString());
                    }
                    dr.Close();
                    cnx.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                actualiser();
            }

            
        }

        private void rech_TextChanged(object sender, EventArgs e)
        {

        }

        private void rech_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }
    }
}
