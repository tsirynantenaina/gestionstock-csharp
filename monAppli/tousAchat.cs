﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace monAppli
{
    public partial class tousAchat : UserControl
    {
        SqlCommand cmd;
        SqlDataReader dr;
        SqlConnection cnx;
        public tousAchat()
        {
            InitializeComponent();
            cnx = new SqlConnection();
            cnx.ConnectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=F:\C#\monAppli\monAppli\base.mdf;Integrated Security=True";
      
            try
            {
                cnx.Open();
                dataGridView1.Rows.Clear();
                String liste = "SELECT client.* , produit.* , achat.* , AchatID.* FROM client , achat , AchatID ,  produit where client.idCli = achat.idCliAchat and AchatID.idAchat =Achat.IdAchat  and produit.IdProd = Achat.idProdAchat order by Achat.idAchat desc";
                SqlCommand cmd = new SqlCommand(liste, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    dataGridView1.Rows.Add( dr["idProd"].ToString(), dr["designation"].ToString(), dr["pu"].ToString(),
                        dr["qteAchat"].ToString(), dr["sousTotalAchat"].ToString(), dr["dateAchat"].ToString(), dr["nomCli"].ToString());
                }
              
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void tousAchat_Load(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
