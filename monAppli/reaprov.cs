﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace monAppli
{
    public partial class reaprov : UserControl
    {
        SqlCommand cmd;
        SqlDataReader dr;
        SqlConnection cnx;
        public reaprov()
        {
            InitializeComponent();
            cnx = new SqlConnection();
            cnx.ConnectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=F:\C#\monAppli\monAppli\base.mdf;Integrated Security=True";
            try
            {
                cnx.Open();
                //dataGridView1.Rows.Clear();
                String liste = "SELECT produit.idProd , produit.designation , fournisseur.nom_Frns, produit.qte , produit.pu , fournisseur.id_Frns  from produit , fournisseur  where produit.idFrns = fournisseur.id_Frns";
                SqlCommand cmd = new SqlCommand(liste, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(),  dr[2].ToString(),
                        dr[3].ToString(), dr[4].ToString());
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        public void actualiser()
        {
            try
            {
                cnx.Open();
                dataGridView1.Rows.Clear();
                String liste = "SELECT produit.idProd , produit.designation , fournisseur.nom_Frns, produit.qte , produit.pu , fournisseur.id_Frns  from produit , fournisseur  where produit.idFrns = fournisseur.id_Frns";
                SqlCommand cmd = new SqlCommand(liste, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(),  dr[2].ToString(),
                        dr[3].ToString(), dr[4].ToString());
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

        }
        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void reaprov_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                if (dataGridView1.CurrentRow.Cells[0].Value != null)
                {
                    label6.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    label3.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                    label8.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                    label9.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();

                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Valider le reaprovisionnement ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                try
                {
                    
                    cnx.Open();
                    String update = "UPDATE  produit SET qte=(qte + "+qteAjoute.Text+") where idProd=" + label6.Text + "";
                    SqlCommand cmd = new SqlCommand(update, cnx);
                    cmd.ExecuteNonQuery();
                    cnx.Close();
                    actualiser();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
                qteAjoute.Text = "0";
            }
        }
    }
}
