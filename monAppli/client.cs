﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace monAppli
{
    public partial class client : UserControl
    {
        SqlCommand cmd;
        SqlDataReader dr;
        SqlConnection cnx;
        public client()
        {
            InitializeComponent();
            cnx = new SqlConnection();
            cnx.ConnectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=F:\C#\monAppli\monAppli\base.mdf;Integrated Security=True";
            try
            {
                cnx.Open();
                String liste = "SELECT * FROM client";
                SqlCommand cmd = new SqlCommand(liste, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(),
                        dr[3].ToString());
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
        public void actualiser()
        {
            try
            {
                cnx.Open();
                dataGridView1.Rows.Clear();
                String liste = "SELECT * FROM client";
                SqlCommand cmd = new SqlCommand(liste, cnx);
                cmd.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(),
                        dr[3].ToString());
                }
                dr.Close();
                cnx.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "Veuilllez remplir le formulaire puis cliquer 'Ajouter'";
            button2.Enabled = true;
            button3.Enabled = false;
            button4.Enabled = false;
            label6.Text = "";
            textnom.Text = "";
            textAdresse.Text = "";
            textTel.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                cnx.Open();
                String insert = "INSERT INTO client(nomCli,adrsCli,telCli) VALUES('" + textnom.Text + "','" + textAdresse.Text + "','" + textTel.Text + "')";
                SqlCommand cmd = new SqlCommand(insert, cnx);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Donnees Inserer");
                cnx.Close();
                actualiser();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void client_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            if (MessageBox.Show("Modifier donnees?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                try
                {
                    cnx.Open();
                    String update = "UPDATE  client SET nomCli ='" + textnom.Text + "',adrsCli='" + textAdresse.Text + "',telCli='" + textTel.Text + "' where idCli=" + label6.Text + "";
                    SqlCommand cmd = new SqlCommand(update, cnx);
                    cmd.ExecuteNonQuery();
                    cnx.Close();
                    actualiser();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Attention tous les donnees concernant ce client vont etre supprimer", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                cnx.Open();
                String deleteAchatID = " DELETE FROM achatID WHERE idClient=" + label6.Text + "";
                SqlCommand cmd = new SqlCommand(deleteAchatID, cnx);
                cmd.ExecuteNonQuery();
                cnx.Close();
                actualiser();

                cnx.Open();
                String deleteAchat = " DELETE FROM achat WHERE idCliAchat=" + label6.Text + "";
                cmd = new SqlCommand(deleteAchat, cnx);
                cmd.ExecuteNonQuery();
                cnx.Close();
                actualiser();

                cnx.Open();
                String delete = " DELETE FROM client WHERE idCli=" + label6.Text + "";
                cmd = new SqlCommand(delete, cnx);
                cmd.ExecuteNonQuery();
                cnx.Close();
                actualiser();
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentRow != null)
            {
                if (dataGridView1.CurrentRow.Cells[0].Value != null)
                {
                    label6.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
                    textnom.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                    textAdresse.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                    textTel.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                    label1.Text = "ID";
                    button2.Enabled = false;
                    button3.Enabled = true;
                    button4.Enabled = true;

                }
            }
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (textBox1.Text != "")
            {
                try
                {
                    cnx.Open();
                    dataGridView1.Rows.Clear();
                    String liste = "SELECT * FROM client where idCli like '%" + textBox1.Text + "%' or nomCli like '%" + textBox1.Text + "%' or adrsCli like '%" + textBox1.Text + "%' or telCli like '%" + textBox1.Text + "%'";
                    SqlCommand cmd = new SqlCommand(liste, cnx);
                    cmd.ExecuteNonQuery();
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(),
                            dr[3].ToString());
                    }
                    dr.Close();
                    cnx.Close();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
            else {

                try
                {
                    cnx.Open();
                    String liste = "SELECT * FROM client";
                    SqlCommand cmd = new SqlCommand(liste, cnx);
                    cmd.ExecuteNonQuery();
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(),
                            dr[3].ToString());
                    }
                    dr.Close();
                    cnx.Close();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void dataGridView1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (textBox1.Text != "")
            {
                try
                {
                    cnx.Open();
                    dataGridView1.Rows.Clear();
                    String liste = "SELECT * FROM client where idCli like '%" + textBox1.Text + "%' or nomCli like '%" + textBox1.Text + "%' or adrsCli like '%" + textBox1.Text + "%' or telCli like '%" + textBox1.Text + "%'";
                    SqlCommand cmd = new SqlCommand(liste, cnx);
                    cmd.ExecuteNonQuery();
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(),
                            dr[3].ToString());
                    }
                    dr.Close();
                    cnx.Close();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
            else
            {

                try
                {
                    cnx.Open();
                    dataGridView1.Rows.Clear();
                    String liste = "SELECT * FROM client";
                    SqlCommand cmd = new SqlCommand(liste, cnx);
                    cmd.ExecuteNonQuery();
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        dataGridView1.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(),
                            dr[3].ToString());
                    }
                    dr.Close();
                    cnx.Close();

                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
